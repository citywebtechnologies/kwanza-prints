<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\Catalog\CatalogController;
use App\Http\Controllers\Catalog\ProductController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\Orders\OrderController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [CatalogController::class, 'index'])->name('index.page');
Route::get('/index', [CatalogController::class, 'landingPage'])->name('index.landing_page');
Route::get('/home', [CatalogController::class, 'index'])->name('homepage');
Route::get('/category/{slug}', [CatalogController::class, 'catalogByCategory'])->name('catalogs.by_category');
Route::get('/occasion/{slug}', [CatalogController::class, 'catalogByOccasion'])->name('catalogs.by_occasion');
Route::get('/product/{slug}/{productName}', [CatalogController::class, 'productDetails'])->name('catalogs.product.details');
Route::get('/product/search', [CatalogController::class, 'searchProducts'])->name('catalogs.product.search');


Route::get('/auth/login',[LoginController::class, 'loginForm'])->name('auth.login-form');
Route::post('/auth/login',[LoginController::class, 'authenticate'])->name('auth.login');
Route::get('/auth/register',[RegisterController::class, 'registerForm'])->name('auth.register-form');
Route::post('/auth/register',[RegisterController::class, 'register'])->name('auth.register');
Route::get('/auth/reset-password',[ResetPasswordController::class, 'resetPasswordForm'])->name('auth.reset-password-form');

Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
Route::resource('manage/products', ProductController::class);
Route::resource('customer/orders', OrderController::class);
