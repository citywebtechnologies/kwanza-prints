@if(Session::has('success') && Session::get('success') !='' )
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <i class="fa fa-check-square mr-2"></i> {!! Session::get('success') !!}
        <button type="button" class="btn-close float-end" data-dismiss="alert" aria-label="Close"></button>
    </div>
@endif


@if(Session::has('failure') && Session::get('failure') !='' )
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <i class="fa fa-exclamation-triangle mr-2"></i> {!! Session::get('failure') !!}
        <button type="button" class="btn-close float-end" data-dismiss="alert" aria-label="Close"></button>
    </div>
@endif
