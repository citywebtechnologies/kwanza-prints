@extends('layouts.admin')
@section('content')

    <div class="row mt-4">
        <div class="col-12">
            <h4 class="mb-1 h4">Products list</h4>
            <table class="table table-responsive-lg table-borderless table-striped">
                <tr>
                    <td>Photo</td>
                    <td>Product name</td>
                    <td>Category</td>
                    <td>Type</td>
                    <td>Occasion</td>
                    <td>Price</td>
                    <td></td>
                </tr>
                @foreach($products as $product)

                    <tr>
                        <td>
                            @if($product->image!=null)
                                <img src="{{asset('storage/uploads/products/thumbnails/'.$product->image)}}"
                                     height="60"/>
                            @else
                                <img src="{{asset('images/logo-dark.png')}}" height="60"/>
                            @endif
                        </td>
                        <td><a href="{{route('catalogs.product.details',[$product->id,slug($product->name)])}}"
                               target="_blank">{{$product->name}}</a></td>
                        <td>@if($product->productCategory!=null){{$product->productCategory->name}}@endif</td>
                        <td>@if($product->productType!=null){{$product->productType->name}}@endif</td>
                        <td>@if($product->occasion !=null){{$product->occasion->name}}@endif</td>
                        <td>{{number_format($product->price)}}TZS</td>
                        <td><a href="{{route('products.edit',$product)}}">Edit</a></td>
                    </tr>

                @endforeach
            </table>

        </div>
    </div>

@endsection
