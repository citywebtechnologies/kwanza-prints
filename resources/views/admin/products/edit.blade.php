@extends('layouts.admin')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3 class="text-2xl text-gray-900 font-semibold my-3"><i
                        class="fa fa-edit text-primary  mr-2"></i>{{__('forms.title_edit_product_details')}}
                </h3>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                @include('components.alert')
                <form action="{{route('products.update',$product)}}" method="post" class="bg-white p-3"
                      accept-charset="UTF-8" id="createProductForm" enctype="multipart/form-data">
                    @csrf
                    @method('patch')
                    <div class="row">
                        <div class="col-xl-8 col-md-8 col-sm-12">
                            <div class='row'>
                                <div class='col-12 mb-3'>
                                    <label class="col-form-label" for='title'>{{__('forms.label_product_name')}}</label>
                                    <input
                                        class='form-control border @error('name') border-danger @enderror '
                                        id='name' type='text' placeholder='{{__('forms.placeholder_product_name')}}'
                                        name="name" value="{{old('name',$product->name)}}">

                                    @error('name')
                                    <div class=" text-danger text-sm p-2">{{$errors->first('name')}}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6 col-sm-12 mb-3">
                                    <label class="col-form-label" for='price'>{{__('forms.label_price')}}</label>
                                    <input type="number" name="price" id="price"
                                           placeholder="{{__('forms.placeholder_price')}}"
                                           value="{{old('price',$product->price)}}"
                                           class="form-control border  @error('price') border-danger @enderror">

                                    @error('price')
                                    <div class=" text-danger text-sm p-2">{{$errors->first('price')}}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6 col-sm-12 mb-3">
                                    <label class="col-form-label" for='cost'>{{__('forms.label_unit_cost')}}</label>
                                    <input type="number" name="cost" id="cost"
                                           placeholder="{{__('forms.placeholder_unit_cost')}}"
                                           value="{{old('cost',$product->cost)}}"
                                           class="form-control border  @error('cost') border-danger @enderror">
                                    @error('cost')
                                    <div class=" text-danger text-sm p-2">{{$errors->first('cost')}}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class='col-md-6 col-sm-12'>
                                    <label class="col-form-label"
                                           for='isbn'>{{__('forms.label_isbn')}}</label>
                                    <input type="text" name="barcode" id="barcode"
                                           placeholder="{{__('forms.placeholder_isbn')}}"
                                           value="{{old('barcode',$product->barcode)}}"
                                           class="form-control border  @error('isbn') border-danger @enderror">

                                    @error('barcode')
                                    <div class=" text-danger text-sm p-2">{{$errors->first('barcode')}}</div>
                                    @enderror
                                </div>
                                <div class='col-md-6 col-sm-12 '>
                                    <label class="col-form-label"
                                           for='productCategory'>{{__('forms.label_product_category')}}</label>

                                    <select name="productCategory" id="productCategory"
                                            class="form-control border @error('productCategory') border-danger @enderror">
                                        <option value="">{{__('forms.select_product_category')}}</option>
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}"
                                                    @if (old('category',$product->product_category_id) == $category->id) selected @endif>
                                                {{$category->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                    @error('productCategory')
                                    <div class="text-danger text-sm p-2">{{$errors->first('productCategory')}}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="row">
                                <div class='col-md-6 col-sm-12'>
                                    <label class="col-form-label"
                                           for='productType'>{{__('forms.label_product_type')}}</label>

                                    <select name="productType" id="productType"
                                            class="form-control border  @error('productType') border-danger @enderror">
                                        <option value="">{{__('forms.select_product_type')}}</option>
                                        @foreach($productTypes as $type)
                                            <option value="{{$type->id}}"
                                                    @if (old('productType',$product->product_type_id) == $type->id)
                                                    selected @endif>{{$type->name}}</option>
                                        @endforeach
                                    </select>
                                    @error('productType')
                                    <div
                                        class=" text-danger text-sm p-2">{{$errors->first('productType')}}</div>
                                    @enderror
                                </div>
                                <div class='col-md-6 col-sm-12'>
                                    <label class="col-form-label" for='status'>{{__('forms.label_status')}}</label>

                                    <select name="status" id="status"
                                            class="form-control border @error('status') border-danger @enderror">
                                        <option value="">{{__('forms.select_publish_status')}}</option>
                                        <option value="1"
                                                @if (old('status',$product->status) == 1) selected @endif>{{__('contents.status_published')}}</option>
                                        <option value="0"
                                                @if (old('status',$product->status) == 0) selected @endif>{{__('contents.status_unpublished')}}</option>
                                    </select>
                                    @error('status')
                                    <div class="text-danger text-sm p-2">{{$errors->first('status')}}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class='row'>
                                <div class='col-md-6 col-sm-12'>
                                    <label class="col-form-label" for='eventType'>{{__('Event type')}}</label>

                                    <select name="eventType" id="eventType"
                                            class="form-control border  @error('eventType') border-danger @enderror">
                                        <option value="">{{__('forms.select_product_type')}}</option>
                                        @foreach($eventTypes as $type)
                                            <option value="{{$type->id}}"
                                                    @if (old('eventType',$product->event_type_id) == $type->id)
                                                    selected @endif>{{$type->name}}</option>
                                        @endforeach
                                    </select>
                                    @error('eventType')
                                    <div
                                        class=" text-danger text-sm p-2">{{$errors->first('eventType')}}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class='row'>
                                <div class='col-12'>
                                    <label class='col-form-label'>{{__('forms.label_description')}}</label>
                                    <textarea cols="10" rows="3" placeholder="{{__('forms.placeholder_product_desc')}}"
                                              class='form-control border @error(' description') border-danger @enderror'
                                              name="description">{{old('description',$product->description)}}</textarea>
                                    @error('description')
                                    <div class="text-danger text-sm p-2">{{$errors->first('description')}}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-4 col-md-4 col-sm-12">
                            @if($product->image !=null)
                                <img src="{{asset('storage/uploads/products/thumbnails/'.$product->image)}}"/>
                            @endif
                            <div class='row'>
                                <div class='col-12 mb-3'>
                                    <label class="col-form-label" for='title'>{{__('forms.label_product_photo')}}</label>
                                    <input type="file" name="productPhoto" id="productPhoto"/>

                                    @error('productPhoto')
                                    <div class="text-danger text-sm p-2">{{$errors->first('productPhoto')}}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12">
                            <button class="btn btn-primary text-white" type="submit">{{__("forms.btn_save_changes")}}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
@endsection
