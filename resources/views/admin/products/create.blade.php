@extends('layouts.employee')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3 class="text-2xl text-gray-900 font-semibold my-2"><i
                        class="fa fa-edit text-primary mr-2"></i>{{__('forms.title_product_details')}}
                </h3>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <form action="{{route('products.store')}}" method="post" class="bg-white p-3"
                      accept-charset="UTF-8" id="createProductForm" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-xl-8 col-md-8 col-sm-12">
                            <div class='row'>
                                <div class='col-12 mb-2'>
                                    <label class="col-form-label" for='title'>{{__('forms.label_product_name')}}</label>
                                    <input
                                        class="form-control border @error('title') border-danger @enderror "
                                        id='title' type='text' placeholder='{{__('forms.placeholder_product_name')}}'
                                        name="title" value="{{old('title')}}">

                                    @error('title')
                                    <div class=" text-danger text-sm p-2">{{$errors->first('title')}}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6 col-sm-12 mb-2">
                                    <label class="col-form-label" for='price'>{{__('forms.label_price')}}</label>
                                    <input type="number" name="price" id="price"
                                           placeholder="{{__('forms.placeholder_price')}}"
                                           value="{{old('price')}}"
                                           class="form-control border  @error('price') border-danger @enderror">

                                    @error('price')
                                    <div class=" text-danger text-sm p-2">{{$errors->first('price')}}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6 col-sm-12 mb-2">
                                    <label class="col-form-label" for='cost'>{{__('forms.label_unit_cost')}}</label>
                                    <input type="number" name="cost" id="cost"
                                           placeholder="{{__('forms.placeholder_unit_cost')}}"
                                           value="{{old('cost')}}"
                                           class="form-control border  @error('cost') border-danger @enderror">
                                    @error('cost')
                                    <div class=" text-danger text-sm p-2">{{$errors->first('cost')}}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-2">
                                <div class='col-md-6 col-sm-12'>
                                    <label class="col-form-label"
                                           for='isbn'>{{__('forms.label_isbn')}}</label>
                                    <input type="text" name="barcode" id="barcode"
                                           placeholder="{{__('forms.placeholder_isbn')}}"
                                           value="{{old('barcode')}}"
                                           class="form-control border  @error('isbn') border-danger @enderror">

                                    @error('barcode')
                                    <div class=" text-danger text-sm p-2">{{$errors->first('barcode')}}</div>
                                    @enderror
                                </div>
                                <div class='col-md-6 col-sm-12 '>
                                    <label class="col-form-label"
                                           for='originCountry'>{{__('forms.label_from_country')}}</label>

                                    <select name="originCountry" id="originCountry"
                                            class="form-control border @error('originCountry') border-danger @enderror">
                                        <option value="">{{__('forms.select_country')}}</option>
                                        @foreach($countries as $country)
                                            <option value="{{$country->id}}"
                                                    @if (old('country') == $country->id) selected @endif>
                                                {{$country->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                    @error('originCountry')
                                    <div class="text-danger text-sm p-2">{{$errors->first('originCountry')}}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="row">
                                <div class='col-md-6 col-sm-12'>
                                    <label class="col-form-label"
                                           for='productType'>{{__('forms.label_product_type')}}</label>

                                    <select name="productType" id="productType"
                                            class="form-control border  @error('productType') border-danger @enderror">
                                        <option value="">{{__('forms.select_product_type')}}</option>
                                        @foreach($productTypes as $type)
                                            <option value="{{$type->id}}"
                                                    @if (old('productType') == $type->id)
                                                    selected @endif>{{$type->name}}</option>
                                        @endforeach
                                    </select>
                                    @error('productType')
                                    <div
                                        class=" text-danger text-sm p-2">{{$errors->first('productType')}}</div>
                                    @enderror
                                </div>
                                <div class='col-md-6 col-sm-12'>
                                    <label class="col-form-label" for='status'>{{__('forms.label_status')}}</label>

                                    <select name="status" id="status"
                                            class="form-control border @error('status') border-danger @enderror">
                                        <option value="">{{__('forms.select_publish_status')}}</option>
                                        <option value="1"
                                                @if (old('status') == 1) selected @endif>{{__('contents.status_published')}}</option>
                                        <option value="0"
                                                @if (old('status') == 0) selected @endif>{{__('contents.status_unpublished')}}</option>
                                    </select>
                                    @error('status')
                                    <div class="text-danger text-sm p-2">{{$errors->first('status')}}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class='row'>
                                <div class='col-12'>
                                    <label class='col-form-label'>{{__('forms.label_description')}}</label>
                                    <textarea cols="10" rows="3" placeholder="{{__('forms.placeholder_product_desc')}}"
                                              class='form-control border @error(' description') border-danger @enderror'
                                              name="description">{{old('description')}}</textarea>
                                    @error('description')
                                    <div class="text-danger text-sm p-2">{{$errors->first('description')}}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-4 col-md-4 col-sm-12">
                            <div class="row">
                                <div class='col-12'>
                                    <label for='category'>{{__('forms.label_product_category')}}</label>
                                    <select name="category" id="category"
                                            class="form-control border p-3   @error('category') border-danger @enderror">
                                        <option value="">{{__('forms.select_product_category')}}</option>
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}"
                                                    @if (old('category') == $category->id) selected @endif>{{$category->name}}
                                            </option>

                                        @endforeach
                                    </select>
                                    @error('category')
                                    <div class=" text-danger text-sm p-2">{{$errors->first('category')}}</div>
                                    @enderror
                                </div>

                                <div class='col-12 mt-4'>
                                    <div class="row">
                                        <div class="col-12">
                                            <label for='authorsDropdown'
                                                   class="float-left d-inline-block">{{__('forms.label_author')}}</label>

                                            <button type="button" class="btn btn-link pull-right  d-inline-block"
                                                    id="btnShowAddAuthorModal">{{__('forms.btn_add_new_author')}}</button>
                                        </div>
                                    </div>

                                    <select name="author" id="authorsDropdown"
                                            class="form-control border p-3   @error('author') border-danger @enderror">
                                        <option value="">{{__('forms.select_author')}}</option>
                                        @foreach($authors as $author)
                                            <option value="{{$author->id}}"
                                                    @if (old('author') == $author->id) selected @endif>{{$author->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                    @error('author')
                                    <div class=" text-danger text-sm p-2">{{$errors->first('author')}}</div>
                                    @enderror
                                </div>

                                <div class='col-12 mt-4'>
                                    <label for='publishersDropdown'>{{__('forms.label_publisher')}}
                                        <span class="float-end pull-right">
                                        <button type="button" class="btn btn-link" id="btnShowAddPublisherModal">
                                            {{__('forms.btn_add_new_publisher')}}</button></span></label>

                                    <select name="publisher" id="publishersDropdown"
                                            class="form-control border  @error('publisher') border-danger @enderror">
                                        <option value="">{{__('forms.select_book_publisher')}}</option>
                                        @foreach($publishers as $publisher)
                                            <option value="{{$publisher->id}}"
                                                    @if (old('publisher') == $publisher->id) selected @endif>{{$publisher->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                    @error('publisher')
                                    <div class=" text-danger text-sm p-2">{{$errors->first('publisher')}}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12">
                            <button class="btn btn-primary text-white" type="submit">{{__("forms.btn_save_changes")}}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
