@extends('layouts.front_app')
@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-6 p-2 my-4">

                @if($errors)
                    <ul class="mt-4">
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                @endif

                <form class="px-4 py-3" action="{{route('auth.login')}}" method="post">
                    @csrf
                    <div class="form-group">
                        <label class="form-label" for="emailAddress">Email address</label>
                        <input type="email" name="email" id="emailAddress" class="form-control"
                               placeholder="email@example.com"
                               value="{{old('email')}}"/>
                    </div>
                    <div class="form-group">
                        <label for="passwordInput" class="form-label">Password</label>
                        <input type="password" name="password" class="form-control" id="passwordInput"
                               placeholder="Password">
                    </div>
                    <div class="row mt-3">
                        <div class="col-12">
                            <button type="submit" class="btn btn-primary">Sign in</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection
