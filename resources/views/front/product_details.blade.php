@extends('layouts.front_app')
@section('content')
    <section class="section-content my-3">
        <div class="container">
            <nav class="mb-3">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('homepage')}}">Home</a></li>
                    @if($product->category!=null)
                        <li class="breadcrumb-item"><a
                                href="{{route('catalogs.by_category',$product->category->slug)}}">{{$product->category->name}}</a>
                        </li>
                    @endif
                    <li class="breadcrumb-item active" aria-current="page">{{ucfirst($product->name)}}</li>
                </ol>
            </nav>

            <div class="row">
                <div class="col-xl-9 col-md-9 col-sm-12">

                    <main class="card">
                        <div class="row no-gutters">
                            <aside class="col-sm-6 border-right">
                                <article class="gallery-wrap">
                                    <div class="img-big-wrap">
                                        <div>
                                            @if($product->image !=null)
                                                <img src="{{asset('storage/uploads/products/'.$product->image)}}"/>
                                            @endif
                                        </div>
                                        {{-- <div class="img-small-wrap">
                                             @if($product->image !=null)
                                                 <img class="item-gallery"
                                                      src="{{asset('storage/uploads/products/'.$product->image)}}"/>
                                                 <img class="item-gallery"
                                                      src="{{asset('storage/uploads/products/'.$product->image)}}"/>
                                             @endif
                                         </div>--}}
                                    </div>
                                </article>
                            </aside>
                            <aside class="col-sm-6">
                                <article class="card-body">
                                    <h3 class="title mb-3">{{ucfirst($product->name)}}</h3>

                                    <div class="mb-3">
                                        <var class="price h3 text-warning">
                                            <span class="currency">TZS</span><span
                                                class="num"> {{number_format($product->price)}}</span>
                                        </var>
                                    </div>
                                    <dl>
                                        <dt>Description</dt>
                                        <dd><p>{{$product->description}}</p></dd>
                                    </dl>
                                    <dl class="row">
                                        <dt class="col-sm-3">Model#</dt>
                                        <dd class="col-sm-9">12345611</dd>

                                        <dt class="col-sm-3">Color</dt>
                                        <dd class="col-sm-9">Black and white</dd>

                                        <dt class="col-sm-3">Delivery</dt>
                                        <dd class="col-sm-9">Russia, USA, and Europe</dd>
                                    </dl>
                                    <div class="rating-wrap">

                                        <ul class="rating-stars">
                                            <li style="width:80%" class="stars-active">
                                                <i class="fa fa-star"></i> <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i> <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </li>
                                            <li>
                                                <i class="fa fa-star"></i> <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i> <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </li>
                                        </ul>
                                        <div class="label-rating">132 reviews</div>
                                        <div class="label-rating">154 orders</div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <dl class="dlist-inline">
                                                <dt>Quantity:</dt>
                                                <dd>
                                                    <select class="form-control form-control-sm" style="width:70px;">
                                                        <option> 1</option>
                                                        <option> 2</option>
                                                        <option> 3</option>
                                                    </select>
                                                </dd>
                                            </dl>  <!-- item-property .// -->
                                        </div>
                                        <div class="col-sm-7">
                                            <dl class="dlist-inline">
                                                <dt>Size:</dt>
                                                <dd>
                                                    <label class="form-check form-check-inline">
                                                        <input class="form-check-input" name="inlineRadioOptions"
                                                               id="inlineRadio2" value="option2" type="radio">
                                                        <span class="form-check-label">SM</span>
                                                    </label>
                                                    <label class="form-check form-check-inline">
                                                        <input class="form-check-input" name="inlineRadioOptions"
                                                               id="inlineRadio2" value="option2" type="radio">
                                                        <span class="form-check-label">MD</span>
                                                    </label>
                                                    <label class="form-check form-check-inline">
                                                        <input class="form-check-input" name="inlineRadioOptions"
                                                               id="inlineRadio2" value="option2" type="radio">
                                                        <span class="form-check-label">XXL</span>
                                                    </label>
                                                </dd>
                                            </dl>
                                        </div>
                                    </div>
                                    <hr>
                                    <button type="button" class="btn btn-outline-success"
                                            data-bs-toggle="modal" data-target="#customizeProductModal">
                                        {{__('Customize')}}</button>
                                </article>
                            </aside>
                        </div>
                    </main>

                    <article class="card mt-3">
                        <div class="card-body">
                            <h4>Detail overview</h4>
                            {!! $product->description !!}
                        </div>
                    </article>
                </div>
                <aside class="col-xl-3 col-md-3 col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            Related items
                        </div>
                        <div class="card-body small">
                            list here
                        </div>
                    </div>
                </aside>
            </div>
        </div>


        <div class="modal fade" id="customizeProductModal" tabindex="-1" aria-labelledby="customizeProductModalLabel"
             aria-hidden="true">

        <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Customize - {{$product->name}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <form method="post" enctype="multipart/form-data" id="customizeProductForm">
                        @csrf
                        <input type="hidden" name="productId" value="{{$product->id}}"/>
                        <div class="modal-body">
                            <div class="mb-3">
                                <label for="" class="form-label">Names</label>
                                <input type="text" class="form-control" name="name"
                                       placeholder="John Doe">
                            </div>

                            <div class="mb-3">
                                <label for="" class="form-label">{{__('Message')}}</label>
                                <textarea class="form-control" name="message" rows="2"></textarea>
                            </div>

                            <div class="mb-3">
                                <label for="" class="form-label">{{__('Additional message')}}</label>
                                <textarea class="form-control" name="additionalMessage" rows="1"></textarea>
                            </div>

                            <div class="mb-3">
                                <label for="" class="form-label">{{__('Comments')}}</label>
                                <textarea class="form-control" name="comment" rows="1"></textarea>
                            </div>

                            <div class="mb-3">
                                <label for="" class="form-label">{{__('Custom photo')}}</label>
                                <input type="file" class="form-control" name="customProductPhoto"/>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light"
                                    data-dismiss="modal">{{__('forms.btn_close')}}</button>
                            <button type="submit" class="btn btn-primary">{{__('forms.btn_save_changes')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <script type="text/javascript">

        $(document).ready(function () {

            $('#customizeProductForm').on('submit', function (event) {
                event.preventDefault();

                const formData = new FormData($(this)[0]);

                $.ajax({
                        url: '{{route('orders.store')}}',
                        type: 'post',
                        method: 'post',
                        data: formData,
                        contentType: false,
                        processData: false,
                        success: (data) => {
                            console.log(data);
                        },
                        beforeSend: () => {
                            console.log('Before send called')
                        }
                    }
                );
            })
            ;
        });
    </script>
@endsection
