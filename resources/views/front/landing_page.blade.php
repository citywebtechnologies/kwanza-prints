@extends('layouts.front_app')
@section('content')
    <main>
        <div id="myCarousel" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-indicators">
                <button type="button" data-bs-target="#myCarousel" data-bs-slide-to="0" class="active"
                        aria-current="true"
                        aria-label="Slide 1"></button>
                <button type="button" data-bs-target="#myCarousel" data-bs-slide-to="1" aria-label="Slide 2"></button>
                <button type="button" data-bs-target="#myCarousel" data-bs-slide-to="2" aria-label="Slide 3"></button>
            </div>
            <div class="carousel-inner">
                <div class="carousel-item active">

                    <img src="{{asset('storage/uploads/sliders/valentine.jpg')}}" alt="Valentines discount"/>

                    <div class="container">
                        <div class="carousel-caption text-start">
                            <h1>Example headline.</h1>
                            <p>Some representative placeholder content for the first slide of the carousel.</p>
                            <p><a class="btn btn-lg btn-orange" href="#">Sign up today</a></p>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="{{asset('storage/uploads/sliders/personalized-wedding.jpg')}}"
                         alt="Personalized wedding gifts"/>

                    <div class="container">
                        <div class="carousel-caption">
                            <h1>Another example headline.</h1>
                            <p>Some representative placeholder content for the second slide of the carousel.</p>
                            <p><a class="btn btn-lg btn-orange" href="#">Learn more</a></p>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <img
                        src="{{asset('storage/uploads/sliders/wedding-invitation-doll-personalized-wedding-gifts-28043482661021.jpg')}}"
                        alt="Wedding gifts"/>

                    <div class="container">
                        <div class="carousel-caption text-end">
                            <h1>One more for good measure.</h1>
                            <p>Some representative placeholder content for the third slide of this carousel.</p>
                            <p><a class="btn btn-lg btn-orange" href="#">Browse gallery</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#myCarousel" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#myCarousel" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>


        <!-- Marketing messaging and featurettes
        ================================================== -->
        <!-- Wrap the rest of the page in another container to center all the content. -->

        <div class="container marketing">

            <!-- Three columns of text below the carousel -->
            <div class="row">
                <div class="col-lg-3">
                    <i class="fas fa-birthday-cake fa-4x text-orange bd-placeholder-img rounded-circle m-3"></i>
                    <h2>Weddings</h2>
                    <p>Some representative placeholder content for the three columns of text below the carousel. This is
                        the
                        first column.</p>
                    <p><a class="btn btn-orange" href="#">View details &raquo;</a></p>
                </div><!-- /.col-lg-4 -->
                <div class="col-lg-3">

                    <i class="fas fa-graduation-cap fa-4x text-orange bd-placeholder-img rounded-circle m-3"></i>

                    <h2>Graduations</h2>
                    <p>Another exciting bit of representative placeholder content. This time, we've moved on to the
                        second
                        column.</p>
                    <p><a class="btn btn-orange" href="#">View details &raquo;</a></p>
                </div><!-- /.col-lg-4 -->
                <div class="col-lg-3">
                    <i class="fas fa-gifts fa-4x text-orange bd-placeholder-img rounded-circle m-3"></i>

                    <h2>More Gifts</h2>
                    <p>And lastly this, the third column of representative placeholder content.</p>
                    <p><a class="btn btn-orange" href="#">View details &raquo;</a></p>
                </div><!-- /.col-lg-4 -->
                <div class="col-lg-3">
                    <i class="fas fa-gifts fa-4x text-orange bd-placeholder-img rounded-circle m-3"></i>

                    <h2>More Gifts</h2>
                    <p>And lastly this, the third column of representative placeholder content.</p>
                    <p><a class="btn btn-orange" href="#">View details &raquo;</a></p>
                </div><!-- /.col-lg-4 -->
            </div><!-- /.row -->

            <!-- START THE FEATURETTES -->

            <hr class="featurette-divider">

            <div class="container">
                <header class="section-heading heading-line">
                    <h4 class="title-section bg text-uppercase">Popular items</h4>
                </header>

                <div class="row">
                    @foreach($popularProducts as $product)
                        <div class="col-md-3 col-sm-6 p-2">
                            <div class="card card-product mb-2">
                                <div class="embed-responsive">
                                    <img src="{{asset('storage/uploads/products/thumbnails/'.$product->image)}}"
                                         class="card-img-top embed-responsive-item"/>
                                </div>
                                <figcaption class="info-wrap  p-2">
                                    <h6 class="title text-uppercase text-orange text-center mt-1">
                                        <a href="{{route('catalogs.product.details',[$product->id,slug($product->name)])}}">{{$product->name}}</a>
                                    </h6>
                                    <div class="price-wrap">
                                        <span class="price-new">{{number_format($product->price)}}</span>
                                        @if($product->compare_price !=null)
                                            <del class="price-old">{{number_format($product->compare_price)}}</del>
                                        @endif
                                    </div>
                                </figcaption>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

        </div><!-- /.container -->
    </main>
@endsection
