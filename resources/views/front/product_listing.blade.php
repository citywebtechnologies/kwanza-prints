@extends('layouts.front_app')
@section('content')

    <section class="section-pagetop bg-brown my-3 " style="height: 120px;">
        <div class="container clearfix">
            <h2 class="title-page">{!! $title ?? 'Products list' !!}</h2>

            <nav class="float-left">
                <ol class="breadcrumb bg-white px-3 py-1">
                    <li class="breadcrumb-item"><a href="{{route('index.page')}}">{{__('navigation.nav_item_home')}}</a>
                    </li>
                    @if(isset($category))
                        <li class="breadcrumb-item"><a
                                href="{{route('catalogs.by_category',$category->slug)}}">{{$category->name}}</a>
                        </li>
                    @endif

                    @if(isset($occasion))
                        <li class="breadcrumb-item"><a
                                href="{{route('catalogs.by_occasion',$occasion->slug)}}">{{$occasion->name}}</a>
                        </li>
                    @endif
                    <li class="breadcrumb-item active" aria-current="page">Products</li>
                </ol>
            </nav>
        </div>
    </section>
    <section class="section-content bg padding-y ">
        <div class="container">
            <div class="row">
                <aside class="col-sm-3">

                    <div class="card card-filter">
                        <article class="card-group-item">
                            <header class="card-header">
                                <a class="" aria-expanded="true" href="#" data-toggle="collapse"
                                   data-target="#collapse22">
                                    <i class="icon-action fa fa-chevron-down"></i>
                                    <h6 class="title">By Category</h6>
                                </a>
                            </header>
                            <div style="" class="filter-content collapse show" id="collapse22">
                                <div class="card-body">
                                    <form class="pb-3">
                                        <div class="input-group">
                                            <input class="form-control" placeholder="Search" type="text">
                                            <div class="input-group-append">
                                                <button class="btn btn-primary" type="button"><i
                                                        class="fa fa-search"></i></button>
                                            </div>
                                        </div>
                                    </form>

                                    <ul class="list-unstyled list-lg">
                                        <li><a href="#">Cras justo odio <span
                                                    class="float-right badge badge-light round">142</span> </a></li>
                                        <li><a href="#">Dapibus ac facilisis <span
                                                    class="float-right badge badge-light round">3</span> </a></li>
                                        <li><a href="#">Morbi leo risus <span
                                                    class="float-right badge badge-light round">32</span> </a></li>
                                        <li><a href="#">Another item <span class="float-right badge badge-light round">12</span>
                                            </a></li>
                                    </ul>
                                </div> <!-- card-body.// -->
                            </div> <!-- collapse .// -->
                        </article> <!-- card-group-item.// -->
                    </div> <!-- card.// -->
                </aside> <!-- col.// -->
                <main class="col-sm-9">

                    @foreach($products as $product)
                        <article class="card card-product">
                            <div class="card-body">
                                <div class="row">
                                    <aside class="col-sm-3">
                                        <div class="img-wrap">
                                            @if($product->image!=null)
                                                <img
                                                    src="{{asset('storage/uploads/products/thumbnails/'.$product->image)}}"
                                                    width="200"/>
                                            @else
                                                <img src="{{asset('images/logo.png')}}" width="200"/>
                                            @endif
                                        </div>
                                    </aside>
                                    <article class="col-sm-6">
                                        <a href="{{route('catalogs.product.details',[$product->id,slug($product->name)])}}">
                                            <h4 class="title">{{$product->name}}</h4>
                                        </a>
                                        <div class="rating-wrap  mb-2">
                                            <ul class="rating-stars">
                                                <li style="width:80%" class="stars-active">
                                                    <i class="fa fa-star"></i> <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i> <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                </li>
                                                <li>
                                                    <i class="fa fa-star"></i> <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i> <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                </li>
                                            </ul>
                                            <div class="label-rating">132 reviews</div>
                                            <div class="label-rating">154 orders</div>
                                        </div>
                                        <p>{{$product->description}}</p>
                                        <dl class="dlist-align">
                                            <dt>Type</dt>
                                            <dd>{{$product->productType->name}}</dd>
                                        </dl>
                                        <dl class="dlist-align">
                                            <dt>Category</dt>
                                            <dd>{{$product->productType->name}}</dd>
                                        </dl>
                                        <dl class="dlist-align">
                                            <dt>Occasion</dt>
                                            <dd>{{$product->occasion->name}}</dd>
                                        </dl>
                                    </article>
                                    <aside class="col-sm-3 border-left">
                                        <div class="action-wrap">
                                            <div class="price-wrap h4">
                                                <span class="price"> {{number_format($product->price)}} TZS</span>
                                                @if($product->compare_price!=null)
                                                    <del
                                                        class="price-old">  {{number_format($product->compare_price)}}</del>
                                                @endif
                                            </div>
                                            {{--<p class="text-success">Free shipping</p>--}}
                                            <br>
                                            <p>
                                                {{--<a href="#" class="btn btn-primary"> Buy now </a>--}}
                                                <a href="{{route('catalogs.product.details',[$product->id,slug($product->name)])}}"
                                                   class="btn btn-secondary"> Details </a>
                                            </p>
                                            <a href="#"><i class="fa fa-heart"></i> Add to wishlist</a>
                                        </div>
                                    </aside>
                                </div>
                            </div>
                        </article>
                    @endforeach
                </main>
            </div>
        </div>
    </section>
@endsection
