@extends('layouts.front_app')
@section('content')
    <!-- ========================= SECTION MAIN ========================= -->
    <section class="section-main bg padding-y-sm">
        <div class="container">
            <div class="card">
                <div class="card-body">
                    <div class="row row-sm">
                        <aside class="col-md-3">
                            <h5 class="text-uppercase">Occasions</h5>
                            <ul class="menu-category list-dots">

                                @foreach($eventTypes as $type)
                                    <li>
                                        <a href="{{route('catalogs.by_occasion',$type->slug)}}">{{$type->name}}</a>
                                    </li>
                                @endforeach

                            </ul>

                        </aside>
                        <div class="col-md-6">
                            <!-- ================= main slide ================= -->
                            <div class="owl-init slider-main owl-carousel" data-items="1" data-nav="true"
                                 data-dots="false">
                                <div class="item-slide">
                                    <img src="{{asset('images/banners/slide1.jpg')}}">
                                </div>
                                <div class="item-slide">
                                    <img src="{{asset('images/banners/slide2.jpg')}}">
                                </div>
                                <div class="item-slide">
                                    <img src="{{asset('images/banners/slide3.jpg')}}">
                                </div>
                            </div>
                            <!-- ============== main slideshow .end // ============= -->

                        </div>
                        <aside class="col-md-3">
                            <h6 class="title-bg bg-secondary">Featured Products</h6>
                            <div style="height:280px;">
                                @foreach($featuredProducts as $fp)
                                    <figure class="itemside has-bg border-bottom" style="height: 33%;">
                                        <img class="img-bg" style="max-height: 85px;"
                                             src="{{asset('storage/uploads/products/thumbnails/'.$fp->image)}}">
                                        <figcaption class="p-2">
                                            <a href="{{route('catalogs.product.details',[$fp->id,slug($fp->name)])}}">
                                                <h6 class="title">{{$fp->name}}</h6>
                                            </a>
                                            {{number_format($fp->price)}}
                                        </figcaption>
                                    </figure>
                                @endforeach
                            </div>
                        </aside>
                    </div>
                </div>
            </div>

            {{--<figure class="mt-3 banner p-3 bg-secondary">
                <div class="text-lg text-center white">Useful banner can be here</div>
            </figure>--}}
        </div>
    </section>

    @foreach($eventTypesWithProducts as $event)
        <section class="section-content padding-y-sm bg">
            <div class="container">
                <header class="section-heading heading-line">
                    <h4 class="title-section bg text-uppercase">{{$event->name}}</h4>
                </header>

                <div class="card">
                    <div class="row no-gutters">
                        <div class="col-md-3">

                            <article href="#" class="card-banner h-100 bg2">
                                <div class="card-body zoom-wrap">
                                    <h5 class="title">{{ucfirst($event->name)}}</h5>
                                    <p>{{$event->description}}</p>
                                    <a href="{{route('catalogs.by_occasion',$event->slug)}}" class="btn btn-warning">Explore</a>
                                    @if($event->icon !=null)
                                        <img src="{{asset('images/icons/'.$event->icon)}}" height="200"
                                             class="img-bg zoom-in">
                                    @endif
                                </div>
                            </article>

                        </div>
                        <div class="col-md-9">
                            @php
                                $chunkedProducts = $event->products->chunk(4);
                            @endphp
                            @foreach($chunkedProducts as $chunk)

                                <ul class="row no-gutters border-cols">
                                    @foreach($chunk as $product)
                                        <li class="col-6 col-md-3">
                                            <a href="{{route('catalogs.product.details',[$product->id,slug($product->name)])}}"
                                               class="itembox">
                                                <div class="card-body">
                                                    <p class="word-limit">{{$product->name}}</p>
                                                    @if($product->image!=null)
                                                        <img class="img-sm"
                                                             src="{{asset('storage/uploads/products/thumbnails/'.$product->image)}}"
                                                             height="100"/>
                                                    @else
                                                        <img class="img-sm" src="{{asset('images/logo-dark.png')}}"
                                                             height="60"/>
                                                    @endif
                                                </div>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endforeach


    {{--<section class="section-request bg padding-y-sm">
        <div class="container">
            <header class="section-heading heading-line">
                <h4 class="title-section bg text-uppercase">Request for Quotation</h4>
            </header>

            <div class="row">
                <div class="col-md-8">
                    <figure class="card-banner banner-size-lg">
                        <figcaption class="overlay left">
                            <br>
                            <h2 style="max-width: 300px;">Big boundle or collection of featured items</h2>
                            <br>
                            <a class="btn btn-warning" href="#">Detail info » </a>
                        </figcaption>
                        <img src="images/banners/banner-request.jpg">
                    </figure>
                </div> <!-- col // -->
                <div class="col-md-4">

                    <div class="card card-body">
                        <h5 class="title py-4">One Request, Multiple Quotes.</h5>
                        <form>
                            <div class="form-group">
                                <input class="form-control" name="" type="text">
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <input class="form-control" name="" type="text">
                                    <span class="input-group-btn" style="border:0; width: 0;"></span>
                                    <select class="form-control">
                                        <option>Pieces</option>
                                        <option>Litres</option>
                                        <option>Tons</option>
                                        <option>Grams</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group text-muted">
                                <p>Select template type:</p>
                                <label class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" value="option1">
                                    <span class="form-check-label">Request price</span>
                                </label>
                                <label class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" value="option2">
                                    <span class="form-check-label">Request a sample</span>
                                </label>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-warning">Request for quote</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>--}}

    <section class="section-request bg padding-y-sm">
        <div class="container">
            <header class="section-heading heading-line">
                <h4 class="title-section bg text-uppercase">Popular items</h4>
            </header>

            <div class="row-sm">
                @foreach($popularProducts as $product)
                    <div class="col-md-2">
                        <figure class="card card-product">
                            <div class="img-wrap"><img
                                    src="{{asset('storage/uploads/products/thumbnails/'.$product->image)}}"></div>
                            <figcaption class="info-wrap">
                                <h6 class="title "><a href="{{route('catalogs.product.details',[$product->id,slug($product->name)])}}">{{$product->name}}</a></h6>
                                <div class="price-wrap">
                                    <span class="price-new">{{number_format($product->price)}}</span>
                                    @if($product->compare_price !=null)
                                        <del class="price-old">{{number_format($product->compare_price)}}</del>
                                    @endif
                                </div>
                            </figcaption>
                        </figure>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
