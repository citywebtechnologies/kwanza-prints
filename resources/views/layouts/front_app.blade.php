<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Bootstrap-ecommerce by Vosidiy">

    <title>{{$title ?? 'Kwanza prints - Home of unique customised gifts for our loved ones, awards, plaques, Signs'}}</title>

    <link rel="shortcut icon" type="image/x-icon" href="{{asset('images/favicon.ico')}}">

    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(mix('css/app.css'))}}" rel="stylesheet" type="text/css"/>
    <script src="{{asset('js/bootstrap.min.js')}}" type="text/javascript"></script>

    <script src="https://kit.fontawesome.com/feff13e77c.js" crossorigin="anonymous"></script>
</head>
<body>

<header>
    <nav class="navbar navbar-expand-md navbar-light fixed-top bg-light">
        <div class="container">
            <a class="navbar-brand" href="{{route('index.page')}}">Kwanza Prints</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse"
                    aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav me-auto mb-2 mb-md-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="{{route('index.landing_page')}}">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('products.index')}}">Shop</a>
                    </li>
                </ul>

                <ul class="navbar-nav me-auto mb-2 mb-md-0 d-flex align-items-end align-content-end">
                    <li class="nav-item">
                        <a class="nav-link" href="#"><i class="fas fa-cart-shopping text-orange"></i> My Order</a>
                    </li>
                </ul>

                <form class="d-flex" method="get" action="{{route('catalogs.product.search')}}">
                    <input class="form-control me-2" type="search" placeholder="Search" name="q" aria-label="Search">
                    <button class="btn btn-outline-success" type="submit">Search</button>
                </form>
            </div>
        </div>
    </nav>
</header>


@yield('content')

<!-- FOOTER -->
<div class="container-fluid footer py-4 mt-5 text-white">
    <footer class="container">
        <div class="row">
            <aside class="col-sm-3 col-md-3 white">
                <h5 class="title">Customer Services</h5>
                <ul class="list-unstyled">
                    <li><a href="#">Help center</a></li>
                    <li><a href="#">Money refund</a></li>
                    <li><a href="#">Terms and Policy</a></li>
                    <li><a href="#">Open dispute</a></li>
                </ul>
            </aside>
            <aside class="col-sm-3  col-md-3 white">
                <h5 class="title">My Account</h5>
                <ul class="list-unstyled">
                    <li><a href="{{route('auth.login')}}"> User Login </a></li>
                    <li><a href="#"> User register </a></li>
                    <li><a href="#"> Account Setting </a></li>
                    <li><a href="#"> My Orders </a></li>
                    <li><a href="#"> My Wishlist </a></li>
                </ul>
            </aside>
            <aside class="col-sm-3  col-md-3 white">
                <h5 class="title">About</h5>
                <ul class="list-unstyled">
                    <li><a href="#"> Our history </a></li>
                    <li><a href="#"> How to buy </a></li>
                    <li><a href="#"> Delivery and payment </a></li>
                    <li><a href="#"> Advertice </a></li>
                    <li><a href="#"> Partnership </a></li>
                </ul>
            </aside>
            <aside class="col-sm-3">
                <article class="white">
                    <h5 class="title">Contacts</h5>
                    <p>
                        <strong>Phone: </strong> +255 659 999 917 <br>
                    </p>

                    <div class="btn-group white">

                        <a class="btn btn-youtube text-orange" title="WhatsApp" target="_blank"
                           href="https://wa.me/255659999917?text={{urlencode('Hello Kwanzaprints, I am inquiring for ')}}">
                            <i class="fab fa-whatsapp fa-fw fa-2x"></i></a>

                        <a class="btn btn-instagram text-orange" title="Instagram" target="_blank"
                           href="https://www.instagram.com/kwanzaprints/"><i
                                class="fab fa-instagram fa-fw fa-2x text-orange"></i></a>

                        <a class="btn btn-facebook text-orange" title="Facebook" target="_blank"
                           href="https://www.facebook.com/KwanzaPrints"><i
                                class="fab fa-facebook-f fa-fw fa-2x text-orange"></i></a>


                        <a class="btn btn-twitter text-orange" title="Twitter" target="_blank" href="#"><i
                                class="fab fa-twitter fa-fw fa-2x"></i></a>
                    </div>
                </article>

                {{--<div class="col-12">
                    <ul class="list-inline">
                        <li class="list-inline-item"><i class="fa fa-whatsapp fa-1x text-orange"></i></li>
                        <li class="list-inline-item"><i class="fa fa-facebook fa-1x text-orange"></i></li>
                        <li class="list-inline-item"><i class="fa fa-instagram fa-1x text-orange"></i></li>
                        <li class="list-inline-item"><i class="fa fa-twitter fa-1x text-orange"></i></li>
                    </ul>
                </div>--}}
            </aside>
        </div>

        <div class="row">
            <div class="col-12">
                <p class="float-end"><a href="#">Back to top</a></p>
                <p> {{date('Y')}} &copy; KwanzaPrints &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a>
                </p>
            </div>
        </div>
    </footer>
</div>
</body>
</html>
