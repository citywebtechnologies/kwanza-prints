<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Bootstrap-ecommerce by Vosidiy">

    <title>{{$title ?? 'Kwanza prints - Home of unique customised gifts for our loved ones, awards, plaques, Signs'}}</title>
    <script src="{{asset('js/jquery-2.0.0.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/bootstrap.bundle.min.js?v=1.01')}}" type="text/javascript"></script>
    <link href="{{asset('css/bootstrap-custom.css')}}" rel="stylesheet" type="text/css"/>
</head>
<body>

<div class="container">

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="{{route('dashboard')}}">Dashboard</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="{{route('products.index')}}">Manage products</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('orders.index')}}">{{__('Customer Orders')}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('index.page')}}" target="_blank">View site</a>
                </li>
                {{--<li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        Dropdown link
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </li>--}}
            </ul>
        </div>
    </nav>

</div>

<div class="container">
    @yield('content')
</div>

<div class="container">
    <a href="">&nbsp; {{date('Y')}} Kwanzaprints</a>
</div>

</body>
</html>
