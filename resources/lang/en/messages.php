<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Success/fail Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'please_wait'                        => 'Please wait',
    'cart_updated'                       => 'Cart updated successfully',
    'available_quantity_is_less'         => 'You can only order :quantity item | You can only order :quantity items',
    'cart_product_removed'               => 'Product removed successfully',
    'order_placed'                       => 'Your order has been placed',
    'search_item_not_found'              => 'The item you are looking for was not found',
    'not_found'                          => 'Not found',
    'shopping_cart_empty'                => 'Your shopping cart is empty',
    'success_coupon_added'               => 'Coupon added successfully',
    'success_coupon_applied'             => 'Coupon applied',
    'failure_coupon_not_added'           => 'Coupon not added',
    'failure_coupon_expired'             => 'Coupon has expired',
    'failure_coupon_exceeded_max_uses'   => 'Coupon has exceeded max uses',
    'failure_coupon_max_usage_exhausted' => 'You have exhausted maximum usage',
    'failure_coupon_already_used'        => 'This coupon is already used',
    'failure_coupon_is_invalid'          => 'Invalid coupon',
    'login_required'                     => 'Login is required to continue',
    'saved_for_later'                    => 'Saved for later',
    'validation_fill_required_fields'    => 'Fill all required fields',
    'success_added_to_wishlist'          => 'Product was added to your wishlist',
    'failure_already_in_wishlist'        => 'Product is already your wishlist',
    'success_removed_from_wishlist'      => 'Product removed from your wishlist',
    'failure_not_in_wishlist'            => 'Product is not in your wishlist',
    'you_have_n_new_notifications'       => 'You have :count new notification| You have :count new notifications'
];
