<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Success/fail Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'please_wait'                        => 'Tafadhali subiri',
    'cart_updated'                       => 'Kikapu kimesasishwa kwa mafanikio',
    'available_quantity_is_less'         => 'Kuna kitabu :quantity tu|Kuna vitabu :quantity tu',
    'cart_product_removed'               => 'Kitabu kimetolea kwa ukamilifu',
    'order_placed'                       => 'Oda yako imewekwa kikamilifu',
    'search_item_not_found'              => 'Bidhaa/kitabu unachotafuta hakikupatikana',
    'not_found'                          => 'Haikupatikana',
    'shopping_cart_empty'                => 'Kikapu chako ni tupu',
    'success_coupon_added'               => 'Koponi imeongezwa kwa mafanikio',
    'success_coupon_applied'             => 'Koponi imetumika',
    'failure_coupon_not_added'           => 'Koponi haikutumika',
    'failure_coupon_expired'             => 'Koponi imefikia muda wa kikomo',
    'failure_coupon_exceeded_max_uses'   => 'Koponi imezidi matumizi ya juu',
    'failure_coupon_max_usage_exhausted' => 'Umemaliza matumizi ya kiwango cha juu',
    'failure_coupon_already_used'        => 'Kuponi hii tayari imetumika',
    'failure_coupon_is_invalid'          => 'Koponi batili',
    'login_required'                     => 'Ingia ili kuendelea',
    'saved_for_later'                    => 'Imehifadhiwa kwa ajili ya baadae',
    'validation_fill_required_fields'    => 'Jaza sehemu zote zinazohitajika',
    'success_added_to_wishlist'          => 'Kitabu kimeongezwa kwenye matakwa yako',
    'failure_already_in_wishlist'        => 'Kitabu tayari kipo kwenye matakwa yako',
    'success_removed_from_wishlist'      => 'Kitabu kimetolewa kwenye matakwa yako',
    'failure_not_in_wishlist'            => 'Bidhaa haipo kwenye orodha ya matakwa yako',
    'you_have_n_new_notifications'       => 'Una ujumbe :count mpya| Una jumbe :count mpya'
];
