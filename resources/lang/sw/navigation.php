<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Forms Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'nav_item_home'                     => 'Nyumbani',
    'nav_item_login'                    => 'Ingia',
    'nav_item_logout'                   => 'Toka',
    'nav_item_account'                  => 'Akaunti',
    'nav_item_register'                 => 'Jisajili',
    'nav_item_browse_store'             => 'Endelea kuperuzi',
    'nav_item_dashboard'                => 'Dashibodi',
    'nav_item_products'                 => 'Vitabu',
    'nav_item_product_requests'         => 'Maombi ya vitabu',
    'nav_item_orders'                   => 'Oda za vitabu',
    'nav_item_purchases'                => 'Manunuzi',
    'nav_item_customers'                => 'Wateja',
    'nav_item_publishers'               => 'Wachapaji',
    'nav_item_coupons'                  => 'Koponi',
    'nav_item_rates_fees'               => 'Gharama za usafirishaji',
    'nav_item_sms_templates'            => 'SMS Templates',
    'nav_item_sms_notifications'        => 'Jumbe za Arifa',
    'nav_item_suppliers'                => 'Wasambazaji',
    'nav_item_trouble_signing_in'       => 'Unapata shida kujiunga?',
    'nav_item_sign_out'                 => 'Toka',
    'nav_item_my_account'               => 'Akaunti yangu',
    'nav_item_my_orders'                => 'Oda zangu',
    'nav_item_change_password'          => 'Badili neno siri',
    'nav_item_browse_categories'        => 'Peruzi makundi',
    'nav_item_cart'                     => 'Kikapu cha manunuzi',
    'nav_item_checkout'                 => 'Malipo',
    'nav_item_shipping_address'         => 'Anuani ya kutuma',
    'nav_item_wish_list'                => 'Matakwa',
    'nav_item_order_complete'           => 'Malipo yamekamilika',
    'nav_item_customer_testimonies'     => 'Ushuhuda wa wateja',
    'nav_item_options'                  => 'Chaguzi',
    'nav_item_option_categories'        => 'Makundi ya chaguzi',
    'nav_item_add_new_option'           => 'Ongeza chaguzi mpya',
    'nav_item_product_categories'       => 'Makundi ya bidhaa',
    'nav_item_slideshows'               => 'Slideshows',
    'nav_item_preferences'              => 'Mipangilio',
    'nav_item_product_reviews'          => 'Maoni ya bidhaa',
    'nav_item_book_authors'             => 'Waandishi',
    'nav_item_search_results'           => 'Matokeo ya utafutaji',
    'nav_item_product_request'          => 'Ombi la kitabu/bidhaa',
    'nav_item_add_testimony'            => 'Ongeza ushuhuda',
    'nav_item_members'                  => 'Wanachama',
    'nav_item_subscription_plans'       => 'Vifurushi vya malipo',
    'nav_item_register_member'          => 'Sajili mwanachama',
    'nav_item_new_from_movement_report' => 'Bidhaa mpya kutoka taarifa za mauzo',
    'nav_item_add_shipping_fee'         => 'Ongeza gharama za kutuma',
    'nav_item_library_attendance'       => 'Maudhurio maktaba',
    'nav_item_book_borrowing'           => 'Uazimishaji vitabu',
    'nav_item_record_attendance'        => 'Rekodi mahudhurio',
    'nav_item_create_package'           => 'Ongeza kifurushi',
    'nav_item_member_subscriptions'     => 'Usajili wa wanachama',
    'nav_item_record_borrow'            => 'Rekodi kitabu kilichoazimwa',
    'nav_item_books_inventory'          => 'Vitabu maktaba',
    'nav_item_add_book_to_library'      => 'Ongeza kitabu',
    'nav_item_regions'                  => 'Mikoa',
    'nav_item_add_region'               => 'Ongeza mkoa',
];
