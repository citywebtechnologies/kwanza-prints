<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset'     => 'Nenosiri lako limewekwa upya!',
    'sent'      => 'Tumekutumia kiungo chako cha kuweka upya nenosiri kwa barua pepe!',
    'throttled' => 'Tafadhali subiri kabla ya kujaribu tena.',
    'token'     => 'Hati(token) hii ya kuweka upya nenosiri si sahihi.',
    'user'      => "Hatuwezi kupata mtumiaji aliye na anwani hiyo ya barua pepe.",

];
