<?php

use Illuminate\Support\Str;

if (!function_exists('padLeft')) {
    function padLeft($number): string
    {
        return sprintf("%04d", $number);
    }
}

if (!function_exists('slug')) {
    function slug($string): string
    {
        return Str::slug($string);
    }
}


if (!function_exists('isClientMobilePhone')) {
    function isClientMobilePhone()
    {
        if (isset($_SERVER["HTTP_USER_AGENT"])) {
            return preg_match(
                "/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i",
                $_SERVER["HTTP_USER_AGENT"]
            );
        } else {
            return false;
        }
    }
}

if (!function_exists('replaceLeadingZero')) {
    function replaceLeadingZero($phoneNumber, $countryCode = 255)
    {
        $phoneNumber = str_replace("+", "", $phoneNumber);

        $numArray = preg_split('//', $phoneNumber, -1, PREG_SPLIT_NO_EMPTY);

        if (isset($numArray[0]) && $numArray[0] == 0) {
            $phoneNumber = preg_replace('/^0?/', $countryCode, $phoneNumber);
        }
        return $phoneNumber;
    }
}

if (!function_exists('logException')) {
    function logException(Exception $exception)
    {
        logger($exception->getMessage() . " " . $exception->getFile() . " " . $exception->getLine());
    }
}


if (!function_exists('isJson')) {
    function isJson($string): bool
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}

if (!function_exists('getBigImageUrl')) {
    function getBigImageUrl($product): string
    {
        return ($product->cover_photo_path != null) ? asset('storage/uploads/images/' . $product->cover_photo_path) : asset('img/default-cover.png');
    }
}

if (!function_exists('getThumbnailUrl')) {
    function getThumbnailUrl($product): string
    {
        return ($product->cover_photo_path != null) ? asset('storage/uploads/images/thumbnails/' . $product->cover_photo_path) : asset('img/default-cover-thumbnail.png');
    }
}

if (!function_exists('displayStatus')) {
    function displayStatus($status, $name = null): string
    {
        if ($status == 1) :
            $string = $name != null ? $name : __("contents.status_published");
            $formattedStatus = '<span class="badge bg-success text-white p-2">' . $string . '</span>';
        elseif (strtolower($status) == 'approved') :
            $string = $name != null ? $name : __("contents.status_approved");
            $formattedStatus = '<span class="badge bg-primary text-white p-2">' . $string . '</span>';
        elseif (strtolower($status) == 'closed') :
            $string = $name != null ? $name : __("contents.status_closed");
            $formattedStatus = '<span class="badge bg-success text-white p-2">' . $string . '</span>';
        elseif ($status == 2 || strtolower($status) == 'pending') :
            $string = $name != null ? $name : __("forms.status_pending");
            $formattedStatus = '<span class="badge bg-warning text-dark p-2">' . $string . '</span>';
        else :
            $string = $name != null ? $name : __("contents.status_unpublished");
            $formattedStatus = '<span class="badge bg-danger text-white p-2" >' . $string . '</span >';
        endif;
        return $formattedStatus;
    }
}

if (!function_exists('getAvatarUrl')) {
    function getAvatarUrl($user)
    {
        return $user->profile_photo_path != null ? $user->profile_photo_path : $user->getAvatar();
    }
}

if (!function_exists('getDisplayName')) {
    function getDisplayName($user)
    {
        return $user->name;
    }
}

if (!function_exists('calculateDiscountPercentage')) {
    function calculateDiscountPercentage($originalPrice, $discountedPrice): string
    {
        $decrease = $originalPrice - $discountedPrice;
        return number_format($decrease / $originalPrice * 100, 0);
    }
}

if (!function_exists('displayShortTitle')) {
    function displayShortTitle($title): string
    {
        $length = strlen($title);

        return ($length <= 20) ? $title : substr($title, 0, 20);
    }
}
if (!function_exists('customRequestCaptcha')) {
    function customRequestCaptcha(): Post
    {
        return new Post();
    }
}

if (!function_exists('humanTimeAgo')) {
    function humanTimeAgo($datetime): string
    {
        return Carbon::createFromTimestamp(strtotime($datetime))->diffForHumans();
    }
}

if (!function_exists('toMysqlDateFormat')) {
    function toMysqlDateFormat($dateString, $format = "Y-m-d H:i:s")
    {
        return date($format, strtotime($dateString));
    }
}
if (!function_exists('deleteOldImage')) {
    function deleteOldImage($dir, $filename)
    {
        try {
            $image = $dir . DIRECTORY_SEPARATOR . $filename;

            if (File::exists($image)) {
                File::delete($image);
            }
        } catch (Exception $exception) {
            logException($exception);
        }
    }
}


if (!function_exists('getTypeOfCoupon')) {
    function getTypeOfCoupon($namespace): string
    {
        $object = new $namespace;

        if ($object instanceof PercentOffCoupon) {
            return 'percent_off';
        } elseif ($object instanceof FixedValueCoupon) {
            return 'fixed';
        } elseif ($object instanceof MinimumQuantityCoupon) {
            return 'min_quantity';
        } else {
            return '';
        }
    }
}

if (!function_exists('diffInDays')) {
    function diffInDays($startDate, $endDate)
    {
        $start = Carbon::parse($startDate);
        $end = Carbon::parse($endDate);
        return $start->diffInDays($end);
    }
}

if (!function_exists('beautifyStatus')) {
    function beautifyStatus($status, $statusType = "success"): ?string
    {
        if ($status == '' || $status == null) {
            return null;
        }

        if ($statusType == "success") {
            return '<div class="text-success"><i class="fa fa-check-circle text-success"></i> '
                . $status . '</div>';
        }

        if ($statusType == "info") {
            return '<div class="text-info"><i class="fa fa-info-circle text-info"></i> '
                . $status . '</div>';
        }

        if ($statusType == "warning") {
            return '<div class="text-primary"><i class="fa fa-info-circle text-primary"></i> '
                . $status . '</div>';
        }

        if ($statusType == "danger" || $statusType == "error") {
            return '<div class="text-danger"><i class="fa fa-exclamation-triangle text-danger"></i> '
                . $status . '</div>';
        }

        return '<div class="text-secondary"><i class="fa fa-exclamation-circle text-default"></i> '
            . $status . '</div>';
    }
}

if (!function_exists('getProductSortOptions')) {
    function getProductSortOptions(): string
    {
        return "<select class='form-control nice-select sort-select mr-0' onchange='location = this.options[this.selectedIndex].value;'>
                <option value=''>Default Sorting</option>
                <option value='" . request()->fullUrlWithQuery(['sort' => 'title', 'direction' => 'asc']) . " '
                 " . ((!array_diff(Request::except('q'), ['sort' => 'title', 'direction' => 'asc'])) ? 'selected' : '') . ">
                Name (A - Z)</option>
                <option value='" . request()->fullUrlWithQuery(['sort' => 'title', 'direction' => 'desc']) . "'
                " . (!array_diff(Request::except('q'), ['sort' => 'title', 'direction' => 'desc']) ? 'selected' : '') . ">
                Name (Z - A)</option>
                <option value='" . request()->fullUrlWithQuery(['sort' => 'price', 'direction' => 'asc']) . "'
                " . (!array_diff(Request::except('q'), ['sort' => 'price', 'direction' => 'asc']) ? 'selected' : '') . ">
                Price (Low &gt; High)</option>
                <option value='" . request()->fullUrlWithQuery(['sort' => 'price', 'direction' => 'desc']) . "'
                " . (!array_diff(Request::except('q'), ['sort' => 'price', 'direction' => 'desc']) ? 'selected' : '') . ">
                Price (High &gt; Low)</option>
                <option value='" . request()->fullUrlWithQuery(['sort' => 'rating', 'direction' => 'desc']) . "'
                " . (!array_diff(Request::except('q'), ['sort' => 'rating', 'direction' => 'desc']) ? 'selected' : '') . ">
                Rating (Highest)</option>
                <option value='" . request()->fullUrlWithQuery(['sort' => 'rating', 'direction' => 'asc']) . "'
                " . (!array_diff(Request::except('q'), ['sort' => 'rating', 'direction' => 'asc']) ? 'selected' : '') . ">
                Rating (Lowest)</option>
            </select>";
    }
}

if (!function_exists('shortenDescription')) {
    function shortenDescription($description)
    {
        return substr($description, 0, (strlen($description) > 250 ? 250 : strlen($description)));
    }
}
