<?php

namespace App\Http\Controllers\Catalog;

use App\Http\Controllers\Controller;
use App\Models\Catalog\EventType;
use App\Models\Catalog\Product;
use App\Models\Catalog\ProductCategory;
use App\Models\Catalog\ProductType;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CatalogController extends Controller
{
    public function landingPage()
    {
        $popularProducts = Product::orderBy('views_count', 'DESC')->limit(12)->get();
        return view('front.landing_page',compact( 'popularProducts', ));
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $eventTypes = EventType::active()->get();

        $eventTypesWithProducts = EventType::active()->with([
            'products' => function ($query) {
                $query->where('status', 1)->limit(8);
            }
        ])->limit(3)->get();

        $popularProducts = Product::orderBy('views_count', 'DESC')->limit(12)->get();
        $featuredProducts = Product::whereNotNull('image')->inRandomOrder()->limit(3)->get();

        return view('front.home',
            compact('eventTypes', 'eventTypesWithProducts', 'popularProducts', 'featuredProducts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function catalogByCategory($slug)
    {
        $category = ProductCategory::where('slug', $slug)->first();
        $products = Product::whereHas('category', function ($query) use ($slug) {
            $query->where('slug', $slug);
        })->active()->paginate();

        $title = $category->name.' products';

        return view('front.product_listing', compact('products', 'title', 'category'));
    }

    public function catalogByOccasion($slug)
    {
        $occasion = EventType::where('slug', $slug)->first();
        $products = Product::whereHas('occasion', function ($query) use ($slug) {
            $query->where('slug', $slug);
        })->active()->paginate();

        $title = $occasion->name.' products';

        return view('front.product_listing', compact('products', 'title', 'occasion'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    public function productDetails($id)
    {
        $product = Product::findOrFail($id);
        return view('front.product_details', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function searchProducts(Request $request)
    {

        $q = $request->q;

        $products = Product::where('name', 'like', "%{$q}%")
            ->orWhere('description', 'like', "%{$q}%")
            ->when($request->productType != null, function ($query) use ($request) {
                $query->whereHas('productType', function ($query2) use ($request) {
                    $query2->where('slug', $request->productType);
                });
            })
            ->paginate();
        $title = $q != null ? "Search results for <b>{$q}</b>" : 'Search results';
        return view('front.product_listing', compact('products', 'title'));
    }
}
