<?php

namespace App\Http\Controllers\Catalog;

use App\Http\Controllers\Controller;
use App\Models\Catalog\EventType;
use App\Models\Catalog\Product;
use App\Models\Catalog\ProductCategory;
use App\Models\Catalog\ProductType;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use function PHPUnit\Framework\exactly;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $products = Product::orderBy('updated_at', 'DESC')->paginate();
        return view('admin.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  Product  $product
     * @return Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Product  $product
     * @return Response
     */
    public function edit(Product $product)
    {
        $categories = ProductCategory::all();
        $productTypes = ProductType::all();
        $eventTypes = EventType::all();

        return view('admin.products.edit', compact('product', 'categories', 'productTypes', 'eventTypes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  Product  $product
     * @return Response
     */
    public function update(Request $request, Product $product)
    {
        $request->validate(
            [
                'name'            => 'required',
                'barcode'         => 'required|unique:products,barcode,'.$product->id,
                'cost'            => 'required|min:50|numeric',
                'price'           => 'required|min:50|numeric',
                'description'     => 'nullable',
                'productType'     => 'nullable',
                'productCategory' => 'required',
                'eventType'       => 'nullable',
                'status'          => 'required',
                'productPhoto'    => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:1024',
            ]
        );

        DB::beginTransaction();
        try {
            $imageName = $this->saveProductPhoto($request);

            $newDetails = [
                'name'                => $request->name,
                'cost'                => $request->cost,
                'price'               => $request->price,
                'barcode'             => $request->barcode,
                'event_type_id'       => $request->eventType,
                'product_category_id' => $request->productCategory,
                'status'              => $request->status
            ];

            if ($request->productType != null) {
                $newDetails['product_type_id'] = $request->productType;
            }

            if ($request->productCategory != null) {
                $newDetails['product_category_id'] = $request->productCategory;
            }

            if ($imageName != null) {
                $newDetails['image'] = $imageName;
            }
            $previousProductImage = $product->image;

            $updateResult = Product::where(['id' => $product->id])->update($newDetails);
            if ($updateResult) {
                if ($imageName != null && $previousProductImage != null) {
                    $this->deleteOlderImage($previousProductImage);
                }
            }

            DB::commit();
            $this->messages['success'] = 'Product was successfully updated';
            return redirect(route('products.index'))->with($this->messages);
        } catch (Exception $exception) {
            DB::rollBack();
            logException($exception);
            $this->messages['failure'] = 'Failed to update product '.$exception->getMessage();
            return redirect()->back()->withInput()->with($this->messages);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Product  $product
     * @return Response
     */
    public function destroy(Product $product)
    {
        //
    }

    private function saveProductPhoto(Request $request)
    {
        $imageName = null;
        try {
            if ($request->productPhoto != null) {
                $uploadDir = storage_path('app/public/uploads/products');
                $thumbnailsDir = $uploadDir.DIRECTORY_SEPARATOR.'thumbnails';
                if (!File::isDirectory($thumbnailsDir)) {
                    File::makeDirectory($uploadDir, 0777, true, true);
                    File::makeDirectory($thumbnailsDir, 0777, true, true);
                }

                $imageName = str_replace('-', '', Str::uuid()->toString()).'.'.$request->productPhoto->extension();
                $image = $request->file('productPhoto');

                $img = $thumbnail = Image::make($image->path());
                //$img->opacity(0);
                $img->resize(500, null, function ($const) {
                    $const->aspectRatio();
                })->save($uploadDir.DIRECTORY_SEPARATOR.$imageName);

                $thumbnail->resize(400,null, function ($const) {
                    $const->aspectRatio();
                })->save($thumbnailsDir.DIRECTORY_SEPARATOR.$imageName);
            }
        } catch (Exception $exception) {
            logException($exception);
        }
        return $imageName;
    }

    private function deleteOlderImage(string $filename)
    {
        try {
            $image = storage_path('app/public/uploads/products').DIRECTORY_SEPARATOR.$filename;
            $thumbnail = $image.DIRECTORY_SEPARATOR.'thumbnails'.DIRECTORY_SEPARATOR.$filename;
            if (File::exists($image)) {
                File::delete($image);
                @File::delete($thumbnail);
            }
        } catch (Exception $exception) {
            logException($exception);
        }
    }
}
