<?php

namespace App\Http\Controllers\Orders;

use App\Http\Controllers\Controller;
use App\Models\Catalog\Product;
use App\Models\Customers\Customer;
use App\Models\Orders\Order;
use App\Models\Orders\OrderDetail;
use App\Models\Orders\OrderItem;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use mysql_xdevapi\Exception;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {

        DB::beginTransaction();
        try {
            $user = Auth::user();

            $product = Product::find($request->productId);

            $customer = Customer::where('user_id', $user->id)->first();
            if ($customer == null) {
                $customer = Customer::create(['user_id' => $user->id]);
            }

            $quantity = 1;

            $order = Order::create([
                'amount'         => $product->price * $quantity,
                'payment_status' => 'PENDING',
                'customer_id'    => $customer->id,
                'order_number'   => Str::random(6)
            ]);

            OrderItem::create([
                'product_id' => $product->id,
                'order_id'   => $order->id,
                'quantity'   => $quantity,
                'price'      => $product->price
            ]);

            $customPhoto = $this->saveCustomerPhoto($request);

            OrderDetail::create([
                'order_id'           => $order->id,
                'customer_id'        => $customer->id,
                'message'            => $request->message,
                'additional_message' => $request->additionalMessage,
                'customer_photo'     => $customPhoto,
                'comments'           => $request->comments,
            ]);

            DB::commit();
            return response()->json(['status' => 'success']);

        } catch (\Exception $exception) {
            DB::rollBack();
            logException($exception);
            return response()->json(['status' => 'failed', 'message' => $exception->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  Order  $customerOrder
     * @return Response
     */
    public function show(Order $customerOrder)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Order  $customerOrder
     * @return Response
     */
    public function edit(Order $customerOrder)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  Order  $customerOrder
     * @return Response
     */
    public function update(Request $request, Order $customerOrder)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Order  $customerOrder
     * @return Response
     */
    public function destroy(Order $customerOrder)
    {
        //
    }

    private function saveCustomerPhoto(Request $request)
    {
        $imageName = null;
        if ($request->customProductPhoto != null) {
            $uploadDir = storage_path('app/public/uploads/orders');
            if (!File::isDirectory($uploadDir)) {
                File::makeDirectory($uploadDir, 0777, true, true);
            }
            $imageName = str_replace('-', '', Str::uuid()->toString()).'.'.$request->customProductPhoto->extension();
            //$request->coverImage->move($uploadDir, $imageName);

            $image = $request->file('customProductPhoto');

            $img = Image::make($image->path());
            $img->resize(500, null, function ($const) {
                $const->aspectRatio();
            })->save($uploadDir.DIRECTORY_SEPARATOR.$imageName);
        }
        return $imageName;
    }
}
