<?php

namespace App\Providers;

use App\Models\Catalog\ProductCategory;
use App\Models\Catalog\ProductType;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //todo restrict to only views urls
        $productCategories = ProductCategory::active()->get();
        $productTypes = ProductType::active()->get();
        View::share(compact('productTypes', 'productCategories'));
    }
}
