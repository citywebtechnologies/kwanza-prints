<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventType extends Model
{
    use HasFactory;

    public function scopeActive($q)
    {
        return $q->where('status', 1);
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'event_type_id');
    }
}
