<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $with = ['category', 'productType', 'occasion'];

    public function productCategory()
    {
        return $this->belongsTo(ProductCategory::class, 'product_category_id');
    }

    public function category()
    {
        return $this->productCategory();
    }

    public function productType()
    {
        return $this->belongsTo(ProductType::class, 'product_type_id');
    }

    public function occasion()
    {
        return $this->belongsTo(EventType::class, 'event_type_id');
    }

    public function eventType()
    {
        return $this->occasion();
    }

    public function scopeActive($q)
    {
        return $q->where('status', 1);
    }
}
