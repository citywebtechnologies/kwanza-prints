<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('barcode')->unique();
            $table->string('name');
            $table->double('cost')->nullable();
            $table->double('price');
            $table->double('compare_price')->nullable()->comment('Price before discount');
            $table->text('description')->nullable();
            $table->string('image')->nullable()->unique();
            $table->foreignId('product_category_id')->references('id')->on('product_categories');
            $table->foreignId('product_type_id')->references('id')->on('product_categories');
            $table->foreignId('event_type_id')->nullable()->references('id')->on('event_types');
            $table->boolean('status')->default(true);
            $table->double('weight')->nullable();
            $table->enum('weight_unit', ['ton', 'kg', 'g', 'mg'])->nullable();
            $table->boolean('featured')->default(false);
            $table->integer('views_count')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
