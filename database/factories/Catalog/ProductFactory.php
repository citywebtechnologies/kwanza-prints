<?php

namespace Database\Factories\Catalog;

use App\Models\Catalog\EventType;
use App\Models\Catalog\Product;
use App\Models\Catalog\ProductCategory;
use App\Models\Catalog\ProductType;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'barcode'             => 'KP'.padLeft($this->faker->unique->randomNumber(8)),
            'name'                => $this->faker->unique->words(mt_rand(2, 5), true),
            'description'         => $this->faker->paragraph,
            'cost'                => 2000 * $this->faker->randomDigit(),
            'price'               => 5000 * $this->faker->randomDigit(),
            'product_category_id' => ProductCategory::inRandomOrder()->first(),
            'product_type_id'     => ProductType::inRandomOrder()->first(),
            'event_type_id'       => EventType::inRandomOrder()->first(),
            'status'              => mt_rand(0, 1)
        ];
    }
}
