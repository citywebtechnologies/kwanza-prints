<?php

namespace Database\Factories\Catalog;

use App\Models\Catalog\ProductType;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductTypeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProductType::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'   => $this->faker->unique->word,
            'slug'   => $this->faker->unique->word,
            'status' => $this->faker->randomElement([1, 2])
        ];
    }
}
