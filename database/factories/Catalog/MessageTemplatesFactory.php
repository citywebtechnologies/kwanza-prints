<?php

namespace Database\Factories\Catalog;

use App\Models\Catalog\MessageTemplate;
use Illuminate\Database\Eloquent\Factories\Factory;

class MessageTemplatesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MessageTemplate::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
