<?php

namespace Database\Seeders;

use App\Models\Catalog\EventType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class EventTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $eventTypes = [
            'Birthdays', 'Anniversaries', 'Babyshower', 'Engagement', 'Weddings',
            'Kitchen party', 'Valentine', 'Retirement Dinner',
            'Farewell', 'Tribute Events', 'Sendoff', 'Graduation'
        ];

        foreach ($eventTypes as $type) {
            EventType::factory()->create(['name' => $type, 'slug' => Str::slug($type)]);
        }
    }
}
