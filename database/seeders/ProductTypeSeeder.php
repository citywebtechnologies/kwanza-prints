<?php

namespace Database\Seeders;

use App\Models\Catalog\ProductType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ProductTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            'Desk organizer', 'Photo frame', 'Kitchen (key holders)',
            'Wallets/Purse', 'Kitchen (keys holders)', 'Wooden'
        ];

        foreach ($categories as $cat) {
            ProductType::factory()->create(['name' => $cat, 'slug' => Str::slug($cat)]);
        }
    }
}
