<?php

namespace Database\Seeders;

use App\Models\Catalog\ProductCategory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ProductCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            'Wooden products', 'Leather products', 'Boards', 'Awards',
            'Medals', 'Notebooks', 'Pens', 'Frames', 'Plaques'
        ];

        foreach ($categories as $cat) {
            ProductCategory::factory()->create(['name' => $cat, 'slug' => Str::slug($cat)]);
        }
    }
}
