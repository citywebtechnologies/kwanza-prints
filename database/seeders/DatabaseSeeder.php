<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Testing\Fluent\Concerns\Has;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory([
            'name'              => 'Godluck Akyoo',
            'email'             => 'g_akyoo@yahoo.com',
            'email_verified_at' => now(),
            'password'          => Hash::make('123456'),
            'remember_token'    => Str::random(10),
            'role'              => 'admin'
        ]);

        $this->call(CustomerSeeder::class);
        $this->call(ProductTypeSeeder::class);
        $this->call(ProductCategorySeeder::class);
        $this->call(EventTypeSeeder::class);

        $this->call(ProductSeeder::class);

    }
}
